# Deadline: 20th October 2015 #

## Terms: ##
* Card values:
      * numbered cards(2-9): numerical value of card
      * face card: 10
      * Ace: 1 or 11 whichever in favor

* Blackjack: two card hand with a face card and an ace.

* Soft hand: hand where any ace is counted 11.

## Rules: ##
* Two cards to each player. Player's cards are both face-up. One of dealer's cards is face-up and one is face-down.
* **H (Hit):** Player receives another card. If value of player's hand > 21, player loses irrespective of dealer's hand.
* **S (Stand):** No card for player.
* **D (Double-down):** Double the bet and player receives only one additional card in the game.
* **P (sPlit):** If the player has a pair of cards, split will convert current game into two independent games. Endless resplits allowed. Doubling is allowed after the splits. **Exception in case of pair of Aces:**
        
     * If split, player gets only one additional card per split hand, and is not allowed to resplit. Moreover, if the new card is a face-card, it will not be treated as a blackjack, but as a regular 21.

* Once the player stands, dealer reveals the face-down card. Dealer hits if value of hand < soft 17 or (< 17), else stands.

## Payoffs: ##

* Player has blackjack and dealer doesn't : Profit = 1.5*bet
* Dealer has blackjack and player doesn't: Loss = bet
* Player busted(hand value>21) : Loss = bet
* Dealer busted: Profit = bet
* Dealer's hand value > Player's hand value: Loss = bet
* Dealer's hand value < Player's hand value: Profit = bet
* Dealer's hand value = Player's hand value: No profit or loss