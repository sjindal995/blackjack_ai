#include <iostream>
#include <cstdio>
#include <vector>
#include <cassert>

using namespace std;
struct Card{
	bool isAce;
	int value;
	bool operator==(const Card &card){
		return (isAce && card.isAce) || (value == card.value);
	}
	bool operator<(const Card &card){
		return (value <= card.value);
	}
	bool validate(){
		return (value <=10 && value >=1 && (!isAce || value == 1));
	}
	friend ostream& operator<<(ostream &out,const Card &card){
		if(card.isAce)out<<"Ace";
		else out<<card.value;
		return out;
	}
};
class Hand{

	int bet;
	int hard_value;
	int soft_value;
	bool has_ace;
	public:
		vector<Card> cards;
		Hand(int _bet):bet(_bet),has_ace(false){
		}

		bool isPair(){
			if(cards.size() != 2) return false;
			return cards[0] == cards[1];
		}
		bool isAcePair(){
			return (isPair() && cards[0].isAce);
		}
		int size(){
			return cards.size();
		}
		bool hasAce(){
			return has_ace;
		}

		int getValue(bool soft){
			int value = 0;
			for(auto &card : cards){
				value += card.value;
			}
			if(has_ace && soft){
				value+=10;
			}
			return value;
		}
		void addCard(Card card){
			assert(card.validate() && "invalid card");
			hard_value += card.value;
			soft_value += card.value;
			if(card.isAce){
				if(!has_ace){
					soft_value += 10;
					has_ace = true;
				}
			}

			cards.push_back(std::move(card));
		}

		//splits the hand into 2, returns the other hand. Only possible if hand is a pair

		Hand split(int new_bet){
			assert(isPair() && "can't split a non-pair");
			Hand new_hand(new_bet);
			new_hand.addCard(cards[1]);
			cards.pop_back();
			return new_hand;
		}


		friend ostream& operator<<(ostream &out, const Hand &hand){
			out<<"Bet: "<<hand.bet<<endl;
			out<<"Cards: ";
			for(auto card : hand.cards){
				out<<card<<" ";
			}
				return out;
		}
};