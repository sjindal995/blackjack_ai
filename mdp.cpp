#include <iostream>
#include <cstdio>
#include <vector>
#include <cstdlib>	
#include <string>
#include <sstream>
#include <cmath>
#include <cassert>
#include <fstream>
using namespace std;
class State{
public:
	double value;
	
	struct Neighbor{
		int sum;
		int has_ace;
		int can_split;
		int can_double;
		int terminal;
		int action_no;
		double weight;
	};
	vector<Neighbor> neighbors;
	State():value(0.0){}
	
	void addNeighbor(int sum, int has_ace, int can_split,int can_double, int terminal, int action,double weight){
		neighbors.push_back({sum,has_ace,can_split,can_double,terminal,action,weight});
	}
	
};
int cnt=0;

class MDP{
public:
	State states[50][2][2][2][2];
	double p;
	char action[5];
	MDP(double _p = 4/13.0){
		p = _p;
		action[0] = '-';
		action[1] = 'H';
		action[2] = 'S';
		action[3] = 'D';
		action[4] = 'P';
	}
	void resetValues(){
		for(int i=0;i<50;i++){
			for(int j=0;j<2;j++){
				for(int k=0;k<2;k++){
					for(int l=0;l<2;l++)for(int m=0;m<2;m++) states[i][j][k][l][m].value = 0.0;
				}
			}
		}
	}
	double prob(int card){
		return (card==10)?p:(1-p)/9;
	}

	double computeReward(int sum,int blackjack, int dealer_hand, int dealer_has_ace, int dealer_hand_size){
		double reward = 0;
		if(dealer_hand_size == 1){
			for(int new_card = 1;new_card < 11;new_card++){
				if((!dealer_has_ace) && (new_card == 1) ) reward += prob(new_card) * computeReward(sum,blackjack,dealer_hand + 11,1,dealer_hand_size+1);
				else reward += prob(new_card) * computeReward(sum,blackjack,dealer_hand + new_card,dealer_has_ace,dealer_hand_size+1);
				// cout << "new_card: " << new_card << ", reward: " << computeReward(sum,blackjack,dealer_hand + new_card,dealer_has_ace,dealer_hand_size+1) << endl;
			}
			return reward;
		}
		if(blackjack){
			if(dealer_hand == 21 && dealer_has_ace && (dealer_hand_size == 2)) return 0;
			else return 1.5;
		}
		else{
			if(dealer_hand > 21 && dealer_has_ace) return computeReward(sum,blackjack,dealer_hand - 10, 0, dealer_hand_size);
			else if(dealer_hand > 21 && !dealer_has_ace) return 1;
			else if(dealer_hand == 21 && dealer_has_ace && (dealer_hand_size == 2)){
				// cout << "dealer_blackjack : " << dealer_hand << "_" << dealer_has_ace << "_"<< dealer_hand_size<<endl;
				return -1;
			}
			else if(dealer_hand >= 17){
				if(sum != dealer_hand){
					// cout << "sum : " << sum << ",  dealer_hand: " << dealer_hand << " ==> " << ((sum-dealer_hand)/abs(sum-dealer_hand))<<endl;
					return ((sum-dealer_hand)/abs(sum-dealer_hand));
				}
				else{
					// cout << "sum : " << sum << ",  dealer_hand: " << dealer_hand  << " ::: " << 0 <<endl;
					return 0;
				}
			}
			else{
				for(int new_card = 1;new_card < 11;new_card++){
					if((!dealer_has_ace) && (new_card == 1) ) reward += prob(new_card) * computeReward(sum,blackjack,dealer_hand + 11,1,dealer_hand_size+1);
					else reward += prob(new_card) * computeReward(sum,blackjack,dealer_hand + new_card,dealer_has_ace,dealer_hand_size+1);
				}
			}
		}
		return reward;
	}
	void assignReward(int dealer_hand){
		for(int sum = 2 ; sum <= 21 ; sum++){
			for(int has_ace = 0 ; has_ace < 2 ; has_ace++){
				for(int can_split = 0 ; can_split < 2 ;can_split++){
					for(int can_double = 0 ; can_double < 2; can_double++){
						State &state = states[sum][has_ace][can_split][can_double][1];
						if(sum==11 && has_ace && can_double) state.value = computeReward(sum+10,1,dealer_hand,dealer_hand==11,1);
						else if(sum != 21){
							if(!has_ace || sum > 11) state.value = computeReward(sum,0,dealer_hand,dealer_hand==11,1);
							else if(sum <= 11) state.value = max(computeReward(sum,0,dealer_hand,dealer_hand==11,1),computeReward(sum+10,0,dealer_hand,dealer_hand==11,1));
						}
						else if(sum==21) state.value = computeReward(sum,0,dealer_hand,dealer_hand==11,1);
						
					}
				}
			}
		}
		for(int sum = 22 ; sum <= 32 ; sum++){
			for(int has_ace = 0 ; has_ace < 2 ; has_ace++){
				for(int can_split = 0 ; can_split < 2 ;can_split++){
					for(int can_double = 0 ; can_double < 2; can_double++){
						for(int terminal = 0 ; terminal < 2; terminal++){
							State &state = states[sum][has_ace][can_split][can_double][terminal];
							state.value = -1;
						}
					}
				}
			}
		}

	}
	void makeGraph(){

		//pairs
		for(int sum = 2 ; sum <= 21 ; sum++){
			for(int has_ace = 0 ; has_ace < 2 ; has_ace++){
				for(int can_split = 0 ; can_split < 2 ;can_split++){
					for(int can_double = 0 ; can_double < 2; can_double++){
						for(int terminal = 0 ; terminal < 2 ; terminal++){
							State &state = states[sum][has_ace][can_split][can_double][terminal];
							if(can_split && sum%2 != 0)continue;
							//if(can_double && sum)
							if(terminal == 0){
								// SPLIT
								if(can_split){
									if(sum%2 != 0)continue;			//means invalid state, continue
									int card = sum/2;
									// each state added with twice the reward
									for(int new_card=1;new_card<=10;new_card++){
										if(card == 1 && new_card == 10)continue;
										int ncan_split = card!=1 && new_card==card;
										int nhas_ace = has_ace || new_card == 1;
										state.addNeighbor(card+new_card,nhas_ace,ncan_split,1,card==1,4,2*prob(new_card));
									}
									if(card==1){
										state.addNeighbor(21,1,1,1,1,4,2*prob(10));
									}
								}
								//HIT
								for(int new_card=1;new_card<=10;new_card++){
									int nhas_ace = has_ace || new_card==1;
									state.addNeighbor(sum+new_card,nhas_ace,0,0,0,1,prob(new_card));
								}
								// Double Down
								if(can_double){
									for(int new_card=1;new_card<=10;new_card++){
										int nhas_ace = has_ace || new_card==1;
										state.addNeighbor(sum+new_card,nhas_ace,0,0,1,3,2*prob(new_card));
									}
								}
								// Stand
								state.addNeighbor(sum,has_ace,can_split,can_double,1,2,1);

							}
						}
					}
				}
			}
		}
		// for(int card=1;card<=10;card++){
		// 	// HIT 1
		// 	for(int new_card=1;new_card<=10;new_card++){
		// 		int sum = 2*card+new_card, has_ace = card==1;
		// 		double prob = (new_card==10)?p:(1-p)/9;
		// 		states[sum][has_ace][1][0].addNeighbor(sum,has_ace,0,0,1,prob);
		// 	}

		// 	//Double 3
		// 	for(int new_card=1;new_card<=10;new_card++){
		// 		int sum = 2*card+new_card, has_ace = card==1;
		// 		double prob = (new_card==10)?p:(1-p)/9;
		// 		states[sum][has_ace][1][0].addNeighbor(sum,has_ace,0,1,3,prob);
		// 	}
		// 	//	
		// }
	}


	void valueIteration(){

		double eps = 1e-5,threshold_eps=1e-15;
		int count = 0;
		while(eps > threshold_eps && count < 20000){
			eps = 1e-15;
			for(int sum = 2 ; sum <= 21 ; sum++){
				for(int has_ace = 0 ; has_ace < 2 ; has_ace++){
					for(int can_split = 0 ; can_split < 2 ;can_split++){
						for(int can_double = 0 ; can_double < 2; can_double++){
							for(int terminal = 0 ; terminal < 2; terminal++){
								State &state = states[sum][has_ace][can_split][can_double][terminal];
								//assert(state.value < 2.0);
								double val[5];
								fill(val,val+5,0.0);
								int allowed[5]={0};

								for(auto &neighbor : state.neighbors){
									State &neighbor_state = states[neighbor.sum][neighbor.has_ace][neighbor.can_split][neighbor.can_double][neighbor.terminal];
									val[neighbor.action_no] += neighbor_state.value*neighbor.weight;
									//cout<<neighbor.weight<<endl;
									//assert(neighbor_state.value < 2);
									allowed[neighbor.action_no] = 1;
								}
								double maxi = -1e15; bool is_terminal = true;
								for(int action = 1 ; action < 5; action++){
									if(allowed[action]){	
										maxi = max(val[action],maxi);
										//cout<<val[action]<<endl;
										is_terminal = false;
									}
								}
								if(!is_terminal){
									if(eps != 1e-15){
										eps = max(eps,fabs(state.value-maxi));
									}
									else{
										eps = fabs(state.value-maxi);
									}
									state.value = maxi;
									//cout<<maxi<<endl;
								}
								
							}
						}
					}
				}
			}
			// printf("%.15lf\n",eps);
			// fflush(stdout);
			// //eps = threshold_eps/2;
			count++;
		}
	}
	char getAction(int action_no){

	}
	int bestAction(const State &state){
		double val[5];
		fill(val,val+5,0.0);
		int allowed[5]={0};

		for(auto &neighbor : state.neighbors){
			State &neighbor_state = states[neighbor.sum][neighbor.has_ace][neighbor.can_split][neighbor.can_double][neighbor.terminal];
			val[neighbor.action_no] += neighbor_state.value*neighbor.weight;
			allowed[neighbor.action_no] = 1;
		}
		double maxi = -1e15; bool is_terminal = true;int best_action = 0;

		for(int action = 1 ; action < 5; action++){
			if(allowed[action]){
				if(val[action] > maxi){
					maxi = val[action];
					best_action = action;
				}
				is_terminal = false;
			}
		}
		return best_action;
	}
	void generateTable(){
		char Table[30][20],Table2[20][20],Table3[11][20];
		for(int dealer_card = 2 ; dealer_card <= 11 ; dealer_card++){
			resetValues();
			assignReward(dealer_card);
			valueIteration();
			for(int sum = 5 ; sum <= 19 ; sum++){
				State &state = states[sum][0][0][1][0];
				int best_action = bestAction(state);
				Table[sum][dealer_card] = action[best_action];
			}
			// pair with an Ace
			for(int other_card = 2 ; other_card <= 9 ; other_card++){
				State &state = states[other_card+1][1][0][1][0];
				int best_action = bestAction(state);
				Table2[other_card+1][dealer_card] = action[best_action];
			}
			//pair
			for(int card = 1 ; card <= 10 ; card++){
				State &state = states[card*2][card==1][1][1][0];
				int best_action = bestAction(state);
				Table3[card][dealer_card] = action[best_action];
			}
		}
		ofstream out("Policy.txt");
		for(int sum = 5 ; sum <= 19 ; sum++){
			// printf("%d\t",sum);
			out << sum << "\t";
			for(int dealer_card = 2; dealer_card <= 11 ;  dealer_card++){
				// cout<<Table[sum][dealer_card]<<" ";
				out<<Table[sum][dealer_card]<<" ";
			}
			// cout<<endl;
			out<<endl;
		}
		for(int other_card = 2; other_card <= 9 ; other_card++){
			// printf("A%d\t",other_card);
			out << "A" << other_card << "\t";
			for(int dealer_card = 2; dealer_card <= 11 ;  dealer_card++){
				// cout<<Table2[1+other_card][dealer_card]<<" ";
				out<<Table2[1+other_card][dealer_card]<<" ";
			}
			// cout<<endl;
			out<<endl;
		}
		for(int card = 2 ; card <= 10 ; card++){
			// printf("%d%d\t",card,card);
			out << card << card << "\t";
			for(int dealer_card = 2 ; dealer_card <=11 ; dealer_card++){
				// cout<<Table3[card][dealer_card]<<" ";
				out<<Table3[card][dealer_card]<<" ";
			}
			// cout<<endl;
			out<<endl;
		}
		// printf("AA\t");
		out << "AA\t";
		for(int dealer_card = 2 ; dealer_card <=11 ; dealer_card++){
			// cout<<Table3[1][dealer_card]<<" ";
			out<<Table3[1][dealer_card]<<" ";
		}
	}
};

int main(int argc, char** argv){
	
	double prob = atof(argv[1]);
 	MDP mdp(prob);
 	mdp.resetValues();
 	//mdp.assignReward(5);
 	mdp.makeGraph();
 	mdp.generateTable();
 	//mdp.valueIteration();
 	 //State &state = mdp.states[8][0][0][1][0];
 	 // for(auto &n : state.neighbors){
 	 // 	cout<<n.sum<<" "
 	 // }
 	//cout<<state.value<<endl;
 	// for(int i=0;i<2;i++)
 	// 	for(int j=0;j<2;j++)
 	// 		for(int k=0;k<2;k++)
 	// 			for(int l=0;l<2;l++)
 	// 				cout<<i<<" "<<j<<" "<<k<<" "<<l<<" "<<mdp.states[3][i][j][k][l].value<<endl;
 	
 	// for(auto &neighbor : mdp.states[2][1][1][1][0].neighbors ){
 	// 	cout<<neighbor.sum<<" "<<neighbor.can_split<<" "<<neighbor.action_no<<" "<<neighbor.weight<<endl;
 	// }
 // 	for(int dealer_card=2;dealer_card<=11;dealer_card++){
	//  	mdp.assignReward(dealer_card);
	//  	for(int sum = 2 ; sum <= 21 ; sum++){
	// 		for(int has_ace = 0 ; has_ace < 2 ; has_ace++){
	// 			for(int can_split = 0 ; can_split < 2 ;can_split++){
	// 				for(int can_double = 0 ; can_double < 2; can_double++){
	// 					for(int terminal = 0 ; terminal < 2; terminal++){
	// 						cout << sum << "_" << has_ace << "_" << can_split << "_" << can_double << "_" << terminal << " , " << dealer_card << "  :::>  " << mdp.states[sum][has_ace][can_split][can_double][terminal].value << endl;
	// 					}
	// 				}
	// 			}
	// 		}
	// 	}
	// 	cout << "------------------#####################----------------------" << endl;
	// }
 	// cout << mdp.computeReward(21,0,11,1,1) << endl;
	// cout<<cnt<<endl;
	bool visited[40][2];
}